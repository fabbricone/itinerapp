class CreateStops < ActiveRecord::Migration[6.1]
  def change
    create_table :stops do |t|
      t.integer :position
      t.datetime :time
      t.integer :place_id

      t.timestamps
    end
  end
end
