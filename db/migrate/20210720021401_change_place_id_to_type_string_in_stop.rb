class ChangePlaceIdToTypeStringInStop < ActiveRecord::Migration[6.1]
  def change
    change_column :stops, :place_id, :string
  end
end
