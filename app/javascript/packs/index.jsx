import React from "react";
import { render } from "react-dom";
import App from "../components/App";

document.addEventListener("DOMContentLoaded", () => {
  const div = document.body.appendChild(document.createElement("div"));
  div.setAttribute("id", "main");
  render(<App />, div);
});
