import React from "react";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Stops from './Stops'

class Trips extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      trips: [],
    };
  }
  
  componentDidMount() {
    this.fetchTrips();
  };
  
  fetchTrips = () => {
    const url = "api/v1/trips";
    fetch(url)
      .then((data) => {
        if (data.ok) {
          return data.json();
        }
        throw new Error("Network error.");
      })
      .then((data) => {
        data.forEach((trip) => {
          const newTrip = {
            key: trip.id,
            id: trip.id,
            name: trip.name
          };
  
          this.setState((prevState) => ({
            trips: [...prevState.trips, newTrip],
          }));
        });
      })
      .then(() => {
        if (this.state.trips.length > 0) {
          const trip = this.state.trips[0];
          this.updateSelectedTrip(trip);
        }
      })
      .catch((err) => console.error("Error: " + err));
  };
  
  itemSelected = (key, event) => {
    const tripId = event.target.getAttribute("data-trip-id")
    const selectedTrip = this.state.trips.filter( trip => trip.id == tripId)[0];
    this.updateSelectedTrip(selectedTrip);
  };
  
  updateSelectedTrip = trip => this.props.updateSelectedTrip(trip);

	render() {
    const trip_names = this.state.trips.map(trip => (
      <Dropdown.Item 
        key={trip.key}
        data-trip-id={trip.id}
        onSelect={this.itemSelected}
        >
      {trip.name}
      </Dropdown.Item>)
    )
    const selectedTripName = this.props.selectedTrip ? this.props.selectedTrip.name : "No Trips yet"

    return (
      <>
        <h2>Trips</h2>
        <DropdownButton id="dropdown-basic-button" title={selectedTripName}>
          {trip_names}
        </DropdownButton>
        <Stops stops={this.props.stops[this.props.selectedTrip?.id]} 
          removeStop={this.props.removeStop} />
      </>
    )
	}
}

export default Trips;