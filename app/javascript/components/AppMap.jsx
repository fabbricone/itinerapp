import React from "react";

class AppMap extends React.Component {
  
  constructor(props) {
    super(props)
    this.appMap = null;
    this.state = {
      annotations: [],
    }
    this.strokeColors = ["#dc3545", "#ff8b32", "#0d6efd", "#00ab68"];
  }
  
  componentDidMount() {
    this.initMap();
  }
  
  initMap = () => {
    const center = new mapkit.Coordinate(42.373611, -71.110558),
      span = new mapkit.CoordinateSpan(0.025, 0.025),
      region = new mapkit.CoordinateRegion(center, span);
    
    this.appMap = new mapkit.Map("map", {
      region: region,
      showsCompass: mapkit.FeatureVisibility.Hidden,
      showsZoomControl: true,
      showsMapTypeControl: false
    });
    
    this.appMap.addEventListener("region-change-end", 
      event => this.props.updateMapRegion(this.appMap));
  }
  
  getStrokeColor = (index) => this.strokeColors[index];

	render() {
	  const allAnnotations = this.props.annotationsForStops.slice().concat(this.props.annotationsForPlaces);
	  if (this.appMap) {
	    const polylines = this.props.routes.map(route => route.polyline);
	    polylines.forEach((polyline, index) => {
	      polyline.style = new mapkit.Style({
          lineWidth: 2,
          lineJoin: "round",
          strokeColor: this.getStrokeColor(index)
        });
	    });
	    this.appMap.annotations = allAnnotations;
	    this.appMap.overlays = polylines;
	    if (this.props.coordinateRegion) {
	      this.appMap.setRegionAnimated(this.props.coordinateRegion)
	    }
	  }
	  return (
      <div id="map"></div>
    )
	}
}

export default AppMap;
