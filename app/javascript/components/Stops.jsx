import React from "react";
import { format } from "date-fns";
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Stops extends React.Component {
  
  constructor(props) {
    super(props)
  }
  
  removeStop = (stopId) => {
    return () => {
      this.props.removeStop(stopId);
    }
  }

	render() {
	  let stops = <div>No Stops yet</div>;
    if (this.props.stops) {
      stops = this.props.stops.map(stop => {
    	  const date = new Date(stop.time);
    	  const dateFormatted = format(date, "h:mm a");
        return (
          <Row key={stop.key} className="p-1">
            <Col xs={{ span: 1 }} className="p-0">
              {stop.position}.
            </Col>
            <Col xs={{ span: 9 }} className="p-0">
              {stop.name}
              <div className="small-font">
                {dateFormatted}
              </div>
            </Col>
            <Col xs={{ span: 1 }} className="p-0">
              <Button onClick={this.removeStop(stop.id)} variant="danger" size="sm" >
                <i className="fa fa-times"></i>
              </Button>
            </Col>
          </Row>
        )
      })
    }

    return (
      <>
        <h3 className="mt-3">Stops</h3>
        <div className="p-2" id="stops" >
          {stops}
        </div>
      </>
    )
	}
}

export default Stops;
