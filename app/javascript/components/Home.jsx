import React from "react";
import AppMap from "./AppMap";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Trips from "./Trips";
import Places from "./Places";
import { ZOOM_OUT_FACTOR_LAT, ZOOM_OUT_FACTOR_LNG } from "./Constants"
import { format } from "date-fns";

class Home extends React.Component {
	
	constructor(props) {
		super(props)
		this.state = {
			annotationsForStops: [],
			annotationsForPlaces: [],
      selectedTrip: null,
      stops: {},
      center: {
        latitude: '42.358425',
        longitude: '-71.062009'
      },
      radius: 2000,
      routesForStop: {}
		}
    this.mapkitDirections = new mapkit.Directions();
  
    this.calloutDelegate = {
      calloutContentForAnnotation: (annotation) => {
        const element = document.createElement("div");
        element.className = "callout-content";
        const title = element.appendChild(document.createElement("span"));
        title.textContent = annotation.title;
        title.className = "callout-title mg-r-10 bottom";
        const button = element.appendChild(document.createElement("BUTTON"));
        button.className = "simple btn btn-primary btn-sm";
        button.addEventListener('mouseup', (event) => {
          this.createStopForPlace(annotation.data.place_id);
        })
        const icon = button.appendChild(document.createElement("icon"));
        icon.className = "fa fa-plus";
        const rating = element.appendChild(document.createElement("div"));
        rating.textContent = `Rating: ${annotation.data.rating}`
        return element;
      }
    }
    
    mapkit.init({
      authorizationCallback: function(done) {
        fetch("/token")
          .then(res => res.text())
          .then(token => {
            done(token);
          })
          .catch(error => {
            console.error(error);
          });
      }
    });
	}
	
  updateAnnotationsForPlaces = (places, glyphColor) => {
    const newAnnotations = this.makeAnnotations(places, glyphColor, {
      clusteringIdentifier: "places",
      callout: this.calloutDelegate
    });
    if (newAnnotations.length > 0) {
      const annotationsToBe = this.state.annotationsForPlaces.slice().concat(newAnnotations);
  		this.setState({ annotationsForPlaces: annotationsToBe }, () => {
        this.findBoundingRegion(this.getAllAnnotations());
  		});
    }
  };
  
  updateAnnotationsForStops = (places, glyphColor) => {
    const newAnnotations = this.makeAnnotations(places, glyphColor, {
      clusteringIdentifier: "stops",
      displayPriority: 2
    });
    if (newAnnotations.length > 0) {
  		this.setState({ annotationsForStops: newAnnotations }, () => {
        this.findBoundingRegion(this.getAllAnnotations());
  		}); 
    }
  };
  
  getAllAnnotations = () => this.state.annotationsForPlaces.slice().concat(this.state.annotationsForStops);
  
  makeAnnotations = (places, glyphColor, options) => {
    if (this.anyMissingGeometry(places))
      return [];
    return places.map(place => {
      return new mapkit.MarkerAnnotation(
        new mapkit.Coordinate(place.geometry.location.lat, place.geometry.location.lng),
        { 
          title: place.name,
          data: place,
          clusteringIdentifier: "default",
          glyphText: `${place.rating}`,
          color: glyphColor,
          displayPriority: 1,
          callout: {},
          ...options
        }
      );
    });
  };
  
  anyMissingGeometry = (places) => {
    const placesWithGeometry = places.filter(place => "geometry" in place);
    // if any of the places are missing a geometry, return true
    if (placesWithGeometry.length != places.length)
      return true;
    return false;
  }
  
  findBoundingRegion = (annotations) => {
    let coordinateRegion;
    if (annotations.length < 2 && annotations.length > 0) {
      const span = new mapkit.CoordinateSpan(0.0427584999999997, 0.0965488499999978);
      const center = new mapkit.Coordinate(annotations[0].coordinate.latitude, 
        annotations[0].coordinate.longitude);
      coordinateRegion = new mapkit.CoordinateRegion(center, span);
    } else {
    	let northLat = -180;
    	let southLat = 180;
    	let eastLng = -180;
    	let westLng = 180;
    	annotations.forEach(annotation => {
    		northLat = Math.max(northLat, annotation.coordinate.latitude);
    		southLat = Math.min(southLat, annotation.coordinate.latitude);
    		eastLng = Math.max(eastLng, annotation.coordinate.longitude);
    		westLng = Math.min(westLng, annotation.coordinate.longitude);
    	});
    	const coordinateRegionTight = new mapkit.BoundingRegion(northLat, eastLng, southLat, westLng)
    		.toCoordinateRegion();
  		const spanWider = new mapkit.CoordinateSpan(
  			coordinateRegionTight.span.latitudeDelta*ZOOM_OUT_FACTOR_LAT,
  			coordinateRegionTight.span.longitudeDelta*ZOOM_OUT_FACTOR_LNG);
    	coordinateRegion = new mapkit.CoordinateRegion(coordinateRegionTight.center, spanWider);
    }
  	this.setState({ coordinateRegion: coordinateRegion });
  };
  
  createStopForPlace(placeId) {
    const data = {
      position: this.getCurrentStops().length+1,
      time: this.getTimeForNewPlace(),
      place_id: placeId,
      authenticity_token: window._token
    }
    const url = `api/v1/trips/${this.state.selectedTrip.id}/stops`;
    fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then((data) => {
        if (data.ok) {
        	this.fetchStops();
        	return;
        }
        throw new Error("Network error fetching stops.");
      })
      .catch((err) => console.error("Error: " + err));
  }
  
  getTimeForNewPlace = () => {
    const currentStops = this.getCurrentStops();
    if (currentStops) {
      const lastStop = currentStops[currentStops.length-1];
      const lastStopTime = lastStop ? new Date(lastStop.time) : new Date("2021-07-18T01:07:25.343Z");
      lastStopTime.setHours(lastStopTime.getHours() + 2);
      return lastStopTime.toUTCString();
      const newTimeFormatted = format(newTime, "yyyy-mm-ddTh:mm");
      return newTimeFormatted;
    } else {
      const today = Date.now()
      today.setHours(9);
      return today;
    }
  }
  
  getCurrentStops = () => this.state.stops[this.state.selectedTrip.id];
  
  updateSelectedTrip = trip => { 
    this.setState({ 
      selectedTrip: trip,
      annotationsForStops: [],
      annotationsForPlaces: [],
      coordinateRegion: null,
      routesForStop: {}
    }, () => {
      this.fetchStops();
    }) 
  }
  
  fetchStops = () => {
    const tripId = this.state.selectedTrip.id;
    const url = `api/v1/trips/${tripId}/stops`;
    fetch(url)
      .then((data) => {
        if (data.ok) {
          return data.json();
        }
        throw new Error("Network error.");
      })
      .then((data) => {
        const newStops = data.map( stop => {
          return {
            key: stop.id,
            id: stop.id,
            position: stop.position,
            time: stop.time,
            placeId: stop.place_id,
          };
        });
        this.setNewStops(newStops, this.fetchPlaceDetails);
      })
      .catch((err) => console.error("Error: " + err));
  };
  
  setNewStops = (newStops, callback) => {
    this.setState({ stops: { ...this.state.stops, [this.state.selectedTrip.id]: newStops }}, callback);
  }
  
  fetchPlaceDetails = () => {
    const currentStops = this.getCurrentStops();
    if (!currentStops) {
      return
    }
    const proxyUrl = "https://warm-bayou-69494.herokuapp.com/"
    const apiUrl = "https://maps.googleapis.com/maps/api/place/details/json"
    let url = new URL(proxyUrl + apiUrl)
    currentStops.forEach((stop, index) => {
      const params = {
        key: 'AIzaSyBfSihVkBACYfHpWMx1HLwCftRZK90i1DU',
        placeid: `${stop.placeId}`,
        fields: "name,geometry",
      }
      url.search = new URLSearchParams(params).toString();
      fetch(url)
        .then(data => {
          if (data.ok) {
            return data.json();
          }
          throw new Error("Network error.");
        })
        .then(data => {
          // update state with name, geometry, and rating for each stop
          const place = data.result;
          const currentStopsCopy = this.getCurrentStops().slice();
          const currentStop = currentStopsCopy[index];
          currentStopsCopy[index] = { ...currentStop, 
            name: place.name,
            geometry: place.geometry,
            rating: `${index+1}`
          }
          this.setNewStops(currentStopsCopy, () => {
            this.updateAnnotationsForStops(this.getCurrentStops(), "#0d6efd");
            this.fetchRoutes();
          });
        })
        .catch((err) => console.error("Error: " + err));
    });
  }
  
  fetchRoutes = () => {
    const stops = this.getCurrentStops();
    if (this.anyMissingGeometry(stops)) {
      return;
    }
    stops.forEach((originStop, index) => {
      if (index !== stops.length-1) {
        const destinationStop = stops[index+1];
        if (originStop.geometry && destinationStop.geometry && !(originStop.id in this.state.routesForStop)) {
          const origin = new mapkit.Coordinate(originStop.geometry.location.lat, 
            originStop.geometry.location.lng);
          const destination = new mapkit.Coordinate(destinationStop.geometry.location.lat, 
            destinationStop.geometry.location.lng);
          this.mapkitDirections.route({ 
              origin: origin, 
              destination: destination,
              transportType: mapkit.Directions.Transport.Walking,
              requestsAlternateRoutes: false
            }, 
            (error, data) => { 
              if (data) {
                const route = data.routes[0];
                this.setState({ routesForStop: { ...this.state.routesForStop, [originStop.id]: route } }, () => {});
              } else {
                console.error(error);
                return;
              }
            } 
          );
        }
      }
    });
  };
  
  updateStop = (newValue) => {
    this.setState({ stops: { ...this.state.stops[this.state.selectedTrip.id], 
      name: newValue }});
  }
  
  removeStop = (stopId) => {
    const data = {
      authenticity_token: window._token
    }
    const url = `api/v1/trips/${this.state.selectedTrip.id}/stops/${stopId}`;
    fetch(url, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then((data) => {
        if (data.ok) {
        // we would like to avoid making Google API requests, but after a delete,
        // we need to re-order the positions of stops
        // we can cache the Google Place info on the page
        // 	this.setNewStops(this.getCurrentStops().filter(stop => stop.id !== stopId),
        // 	  () => {});
          this.setState({ routesForStop: {} }, () => {
            this.fetchStops();
          })
        	return;
        }
        throw new Error("Network error.");
      })
      .catch((err) => console.error("Error: " + err));
  }
  
  updateMapRegion = (map) => {
    this.setState({
      center: map.region.center,
      radius: map.region.radius,
      coordinateRegion: null
    });
  }

	render() {
		return (
			<Container fluid className="full-height">
			  <Row className="full-height">
			    <Col md={3} className="p-0" id="search-gutter">
			    	<Places updateAnnotations={this.updateAnnotationsForPlaces.bind(this)} 
			    		createStopForPlace={this.createStopForPlace.bind(this)} 
			    		center={this.state.center} 
			    		radius={this.state.radius} />
			    </Col>
			    <Col className="g-0">
			    	<AppMap annotationsForPlaces={this.state.annotationsForPlaces}
			    	  annotationsForStops={this.state.annotationsForStops}
			    		coordinateRegion={this.state.coordinateRegion}
			    		updateMapRegion={this.updateMapRegion.bind(this)}
			    		routes={Object.values(this.state.routesForStop)}
			    	/>
			    </Col>
			    <Col md={2} id="trips-gutter">
			    	<Trips updateSelectedTrip={this.updateSelectedTrip.bind(this)} 
			    		selectedTrip={this.state.selectedTrip}
			    		stops={this.state.stops}
			    		removeStop={this.removeStop.bind(this)}
			    		/>
			    </Col>
			  </Row>
			</Container>
		)
	}	
}

export default Home;
