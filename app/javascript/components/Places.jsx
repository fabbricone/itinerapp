import React from "react";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faSolid } from '@fortawesome/free-solid-svg-icons'
import { CLIENT_ID, CLIENT_SECRET } from './Secrets';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

class Places extends React.Component {
  
  constructor(props) {
    super(props)
    this.categories = [
      [0, "Arts & Entertainment", "entertainment", "🖼"],
      [1, "Event", "event", "🎭"],
      [2, "Food", "food", "🍽"],
      [3, "Nightlife Spot", "nightlife", "💃"],
      [4, "Outdoors & Recreation", "recreation", "🏃"],
      [5, "Shopping", "shopping", "🛒"],
    ].map(data => this.newCategory(data[0], data[1], data[2], data[3]))
    this.state = {
      selectedCategory: this.categories[0],
      // places: this.getExamplePlaces(),
      places: [],
      keyword: ""
    }
    this.props.updateAnnotations(this.state.places, this.state.selectedCategory.glyph);
    this.searchSubmitted = this.searchSubmitted.bind(this);
  }
  
  newCategory = (key, name, keyword, glyph) => ({
    key: key,
    name: name,
    keyword: keyword,
    glyph: glyph
  });
  
  getExamplePlaces = () => {
    return [
        { place_id: "4b3d563ff964a520a49225e3", name: "Faneuil Hall Marketplace", contact: {}, rating: 3.9,
          geometry: {
            location: {
              lat: 42.359977505576424,
              lng: -71.05641034076862
        }}},
        { place_id: "44cf0e37f964a5201b361fe3", name: "New England Aquarium", contact: {}, rating: 4.5,
          geometry: {
            location: {
              lat: 42.359239509045004,
              lng: -71.0491609173823
        }}},
        { place_id: "40eb3d00f964a520250a1fe3", name: "Fenway Park", contact: {}, rating: 4.3,
          geometry: {
            location: {
              lat: 42.359239509045004,
              lng: -71.05641034076862,
        }}}
      ]
  }
  
  searchSubmitted = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.fetchPlaces();
  };
  
  fetchPlaces = () => {
    const proxyUrl = "https://warm-bayou-69494.herokuapp.com/"
    const apiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    let url = new URL(proxyUrl + apiUrl)
    const params = {
      key: 'AIzaSyBfSihVkBACYfHpWMx1HLwCftRZK90i1DU',
      location: this.getLocation(),
      radius: this.props.radius,
      // keyword: selectedCategory.keyword,
      keyword: this.state.keyword,
    }
    url.search = new URLSearchParams(params).toString();
    fetch(url)
      .then(data => {
        if (data.ok) {
          return data.json();
        }
        throw new Error("Network error.");
      })
      .then(data => {
        const places = data.results;
        this.setState({ places: places });
        this.props.updateAnnotations(places, "black");
      })
      .catch((err) => console.error("Error: " + err));
  };
  
  getLocation = () => `${this.props.center.latitude},${this.props.center.longitude}`;
  
  handleSubmit(placeId) {
    return event => {
      this.props.createStopForPlace(placeId);
    }
  }

	render() {
    const category_names = this.categories.map(category => (
      <Dropdown.Item 
        key={category.key}
        data-item-id={category.key}
        onSelect={this.itemSelected}
        >
      {category.name}
      </Dropdown.Item>)
    )
    const places = this.state.places.map(place => (
      <Row key={place.place_id} style={{ marginLeft: '1%' }} className="pt-1">
        <Col md={1} className="p-0">
          <Button onClick={this.handleSubmit(place.place_id)} className="simple" size="sm">
                <i className="fa fa-plus"></i>
          </Button>
        </Col>
        <Col className="p-1">
          <div>
            {place.name}
          </div>
        </Col>
      </Row>
    ));

    return (
      <>
        {/*
        <div>
          <h3>Categories</h3>
          <DropdownButton id="dropdown-basic-button" title={this.state.selectedCategory.name}>
            {category_names}
          </DropdownButton>
        </div> 
        */ }
        <div className="m-0">
          <Form onSubmit={this.searchSubmitted} >
            <Form.Group>
              <InputGroup>
                <Form.Control type="text" placeholder="Search" 
                  value={this.state.keyword}
                  onChange={e => this.setState({ keyword: e.target.value })}
                />
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </InputGroup>
            </Form.Group>
          </Form>
        </div> 
        <div className="mt-1 pb-1 d-none">
          <div className="full-height ml-2">
            {places}
          </div>
        </div>
      </>
    )
	}
}

export default Places;
