class Api::V1::StopsController < ApplicationController
  before_action :set_stop, only: [:show, :edit, :update, :destroy]
  before_action :set_trip

  def index
    @stops = Stop.order(position: :asc).select {|s| s.trip_id == @trip_id}
    puts @stop
    render json: @stops
  end

  def show
    if @stop
      render json: @stop
    else
      render json: @stop.errors
    end
  end

  def new
    @stop = Stop.new
  end

  def edit
  end

  def create
    @stop = Stop.new(stop_params)

    if @stop.save
      render json: @stop
    else
      render json: @stop.errors
    end
  end

  def update
  end

  def destroy
    stops_to_update = Stop.select {|s| s.trip_id == @stop.trip_id}.select {|s| s.position > @stop.position}
    stops_to_update.each { |stop|
      stop.position -= 1
      stop.save
    }
    @stop.destroy

    render json: { notice: 'Stop was successfully removed.' }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stop
      @stop = Stop.find(params[:id])
    end

    def set_trip
      @trip_id = params[:trip_id].to_i
    end

    # Only allow a list of trusted parameters through.
    def stop_params
      params.permit(:position, :time, :place_id, :trip_id)
    end
end
