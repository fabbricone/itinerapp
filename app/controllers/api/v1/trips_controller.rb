class Api::V1::TripsController < ApplicationController
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  def index
    @trips = Trip.all
    render json: @trips
  end

  def show
    if @trip
      render json: @trip
    else
      render json: @trip.errors
    end
  end

  def new
    @trip = Trip.new
  end

  def edit
  end

  def create
    @trip = Trip.new(trip_params)


    if @trip.save
      render json: @trip
    else
      render json: @trip.errors
    end
  end

  def update
  end

  def destroy
    @trip.destroy

    render json: { notice: 'Trip was successfully removed.' }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def trip_params
      params.permit(:name, :trip_id)
    end
end