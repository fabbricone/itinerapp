require 'jwt'

class MapkitTokenController < ApplicationController
  
  @@key_str = MapkitTokenHelper::TOKEN
  
  def token
    payload = { iss: "96P47M429U", exp: (DateTime.now()+1).to_time.to_i, iat: DateTime.now().to_time.to_i }
    key = OpenSSL::PKey::EC.new(@@key_str)
    header = { typ: "JWT", kid: "Z344YRK9HB" }
    jwt_token = JWT.encode(payload, key, 'ES256', header)
    render plain: jwt_token
  end
end
