Rails.application.routes.draw do
  root 'root#index'
  get "/token", to: 'mapkit_token#token'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :trips do 
        resources :stops 
      end 
      # get 'trips', to: 'trips#index'
      # post 'trips/create'
      # delete 'trips/:id', to: 'trips#destroy'
    end
  end
end
